// Microswitch probe mount for Delta Printers
// Other modules used from Kossel git repo
//  ## Variables ############################################################################# 
tolerance = 0.1;
// mount
mount_thickness = 6; //thickness of aluminium
mount_width = 37;
mount_length = 28+mount_thickness;
mount_depth = 44;
mount_radius = 12.5;
groove_radius = 6;
groove_length = 18;
plate_offset = groove_length-((28/2)-groove_radius*2); //Plate isn't centered around hole, account for that
plate_offset = (groove_length/2-groove_radius);
// Kossel Effector
eff_height = 8;
//  ## End Effector
m3_wide_radius = 1.725;
separation = 40;  // Distance between ball joint mounting faces.
offset = 20;  // Same as DELTA_EFFECTOR_OFFSET in Marlin.
mount_radius = 12.5;  // Hotend mounting screws, standard would be 25mm.
hotend_radius = 8;  // Hole for the hotend (J-Head diameter is 16mm).
push_fit_height = 4;  // Length of brass threaded into printed plastic.
height = eff_height;
cone_r1 = 2.5;
cone_r2 = 14;
//  ## E3D Volcano HotEnd
// resolution of round object. each line segment is fnr mm long. fnr = 1 crude and good for development (its faster), aim for fnr = 0.4 or smaller for a production render. smaller means more details (and a lot more time to render).
fnresolution = 0.4;
// detail level
detaillevel = 0; // [0:coarse render only outlines,1:fine render with all details]
// Probe Top
top_thickness = mount_thickness +4;
traxxas_clearance = 2;
// Cylinder magnets;
magnet_radius = 6+tolerance;
magnet_thickness = 5;
magnet_hole_diameter = 4+tolerance;
magnet_wall = 2;
trap_depth = 1; //Magnets are 1mm thinner than piece, also adjust with receptacle_depth for magnet separation
screw_depth = 20;
// Microswitch
switch_offset = 6; //Z location of switch in mounting 
switch_arm = 10; //thickness of switch mounting
probe_height = 70; //70 has contacts clear of nozzle


//  ## Parts ################################################################################ 

//  ## Probe top
translate([0,0,-eff_height-(top_thickness)]) rotate([0,0,120]) probe_top();    
//  ## Probe Body
translate([0,0,-eff_height-top_thickness-(probe_height/2)]) rotate([0,0,120]) probe_body();
//  ## Microswitch
color("white", 0.5) translate([0,0,-eff_height-top_thickness-(probe_height) + switch_offset]) rotate([0,180,120]) microswitch();


//  ## Effector
translate([0, 0, -eff_height/2]) rotate ([0, 180, 0]) effector();
//  ## Hotend mount
color("white", 1) translate([0,0,-eff_height-(mount_thickness/2)]) rotate ([0, 0, 120]) mount();
//  ## E3D Hotend
//color("blue", 1) 
rotate([0, 180, -60]) translate([0, 0, 1.1]) e3d();

//  ## Modules #############################################################################
//  ## Hotend Mount
module mount()
{
    thickness = 6; //thickness of aluminium
    width = 37;
    length = 28+thickness;
    depth = 44;
    mount_radius = 12.5;
    groove_radius = 6;
    groove_length = 18;
    plate_offset = groove_length-((28/2)-groove_radius*2); //Plate isn't centered around hole, account for that
    plate_offset = (groove_length/2-groove_radius);
    difference() {
    union() {
        difference() {
            // Horizontal Plate
            translate([0,plate_offset,0]) cube([width,length,thickness], center = true);
            // Grove cutout
             translate([-groove_radius,-groove_length,-thickness/2]) cube ([groove_radius*2, groove_length, thickness], center = false);
        }
        // Vertical Plate
        difference() {
            rotate([0,0,0]) translate([0, plate_offset + length/2 - thickness/2, -depth/2]) cube([   width,thickness,depth], center = true);
            // Fan Hole
            translate([0,0,-22]) rotate([90,0,0]) cylinder(h=50,r=15, center=true);
        }
    }
    // Mounting holes for extruder
    for (a = [60,120,240,300]) rotate([0, 0, a]) {translate([0, mount_radius, 0]) cylinder(r=m3_wide_radius, h=2*height, center=true, $fn=12);}
    }
}

//  ## Probe Top attachment plate
module probe_top()
{
    difference()
    {
        //Plastic Block
        hull()
        {
            // Main part
            translate([0,0,top_thickness/2]) cube([mount_width,28,top_thickness], center = true);
            // Magnet wings
            translate([(mount_width/2)+magnet_radius+magnet_wall,0,top_thickness/2]) cylinder(r=magnet_radius+magnet_wall, h=top_thickness, center=true);
        mirror([1,0,0]) translate([(mount_width/2)+magnet_radius+magnet_wall,0,top_thickness/2]) cylinder(r=magnet_radius+magnet_wall, h=top_thickness, center=true);
        }
        // Top Magnet 'traps'
        //Left hole    
        translate([(mount_width/2)+magnet_radius+magnet_wall,0,top_thickness -(magnet_thickness/2) -trap_depth]) difference()
        {
            // Hole
            cylinder(r=magnet_radius, h=top_thickness, center=true);
            // Magnet nub M4 for location and gluing
            cylinder(r=(magnet_hole_diameter/2), h=3, center=true);
        }
        //Right hole    
        mirror([1,0,0]) translate([(mount_width/2)+magnet_radius+magnet_wall,0,top_thickness -(magnet_thickness/2) -trap_depth]) difference()
        {
            // Hole
            cylinder(r=magnet_radius, h=top_thickness, center=true);
            // Magnet nub M4 for location and gluing
            cylinder(r=(magnet_hole_diameter/2), h=3, center=true);
        }
        // Left receptacle hole
        receptacle_depth = 0.5;
        translate([(mount_width/2)+magnet_radius+magnet_wall,0,receptacle_depth]) cylinder(r=magnet_radius, h=receptacle_depth*2 +tolerance, center=true);
        // Right receptacle hole
        mirror([1,0,0]) translate([(mount_width/2)+magnet_radius+magnet_wall,0,receptacle_depth]) cylinder(r=magnet_radius, h=receptacle_depth*2 + tolerance, center=true);
        // Alu mount notch
        translate([0,0,(top_thickness)-(mount_thickness/2)]) cube([mount_width,mount_length,mount_thickness+tolerance], center = true);
        // Mounting holes (nut traps)
        // Mounting holes for extruder
        for (a = [60,120,240,300]) rotate([0, 0, a]) {translate([0, mount_radius, 0])
            union() {
                // M3 holes    
                cylinder(r=m3_wide_radius, h=top_thickness, center=true, $fn=12);
                // Nut trap from http://wiki.hacman.org.uk/OpenSCAD#Nut_Traps_.2F_Hexagons
                cylinder(r=5.5 / 2 / cos(180 / 6) + 0.05, h=4, $fn=6, center=true);
            }
        }    
        // Middle break to split into parts
        union()
        {
            //Nozzle cutout for 16mm diameter fin
            cylinder(r=8+0.3, h=top_thickness, center=true);
            cube([groove_radius*2,mount_length,top_thickness], center=true);
        }
    
        //Reduce top of wings a little to accomodate traxxas mounts with a chamfer
        //Left Traxxas clearance
        clearance_thickness = magnet_radius+magnet_wall+2;
        clearance_cylinder = 20;
        cylinder_adjust = 2;
        translate([mount_width/2+(magnet_radius*2)-cylinder_adjust,-clearance_cylinder/2,top_thickness+(clearance_thickness-traxxas_clearance)]) rotate([90,0,0]) cylinder(r=clearance_thickness, h=clearance_cylinder, center=true);
    //Right Traxxas clearance
    mirror([1,0,0]) translate([mount_width/2+(magnet_radius*2)-cylinder_adjust,-clearance_cylinder/2,top_thickness+(clearance_thickness-traxxas_clearance)]) rotate([90,0,0]) cylinder(r=clearance_thickness, h=clearance_cylinder, center=true);
        
    } //End of difference
}
    
//  ## Probe assembly
module probe_body()
{
    difference()
    {
        
        //Main block
        width = mount_width+(magnet_radius*2 + magnet_wall*2)*2;
        thickness = magnet_radius*2 + magnet_wall*2;
        depth = probe_height;
        cube([width,thickness,depth], center=true);
        //Middle subtraction
        difference()
        {
            angle = 45;
            width_adjust = 12;
            adjust = 6;
            translate([0,0,switch_arm]) cube([mount_width+adjust,thickness+0.1,depth], center=true );
            translate([mount_width/2-thickness/2 + width_adjust,0,switch_arm-(depth/2)]) rotate([0,angle,0]) cube([thickness,thickness+0.1,thickness*2], center = true);
            mirror([1,0,0]) translate([mount_width/2-thickness/2 + width_adjust,0,switch_arm-(depth/2)]) rotate([0,angle,0]) cube([thickness,thickness+0.1,thickness*2], center = true);
        }
        // Bottom Edge chamfers
        width_adjust = 4;
        angle = 45;
        translate([width/2-thickness/2 + width_adjust,0,-depth/2+thickness/2 -width_adjust]) rotate([0,angle,0]) cube([thickness,thickness+0.1,thickness*2], center = true);
        mirror([1,0,0]) translate([width/2-thickness/2 + width_adjust,0,-depth/2+thickness/2 -width_adjust]) rotate([0,angle,0]) cube([thickness,thickness+0.1,thickness*2], center = true);
        
        // Top Magnet 'traps'
        trap_depth = -1; //raise magnets up slightly so they are proud of surface
        //Left hole    
        translate([(mount_width/2)+magnet_radius+magnet_wall,0,depth/2 -(magnet_thickness/2) -trap_depth]) union()
        {
            // Hole
            cylinder(r=magnet_radius, h=top_thickness, center=true);
            // Magnet M4 screw hole
            cylinder(r=(magnet_hole_diameter/2), h=screw_depth*2, center=true);
        }
        //Right hole    
        mirror([1,0,0]) translate([(mount_width/2)+magnet_radius+magnet_wall,0,depth/2 -(magnet_thickness/2) -trap_depth]) union()
        {
            // Hole
            cylinder(r=magnet_radius, h=top_thickness, center=true);
            // Magnet M4 screw hole
            cylinder(r=(magnet_hole_diameter/2), h=screw_depth*2, center=true);
    }
    //Hole for microswitch body
    translate([0,0,-(depth/2) + switch_offset]) rotate([0,180,0]) microswitch();
    // Hole for microswitch contacts
    hole_offset = 8;
    translate([0,0,-(depth/2) + switch_offset+hole_offset]) cube([19.8, 6, 10], center=true);
    //Holes for microswitch bolts
    for (x = [-9.5/2, 9.5/2]) {
        translate([x, 0, -(depth/2) + switch_offset]) rotate([90, 0, 0])
            union()
            {
                //Bolt hole
                cylinder(r=2.5/2+(tolerance), h=20, center=true, $fn=12);
                //Cap Hole
                head_depth = 1.5;
                translate([0,0,(thickness/2)-(head_depth/2)]) cylinder(r=(4.5/2)+tolerance, h=head_depth, center=true);
                //Nut trap
                nut_depth = 2.5;
                translate([0,0,-(thickness/2)+(nut_depth/2)]) cylinder(r=5 / 2 / cos(180 / 6) + 0.05, h=nut_depth, $fn=6, center=true);
            }
    }
    }
}
//  ## Microswitch
module microswitch() {
  difference() {
    union() {
      translate([0, 0, 2.5])
        cube([19.8, 6, 10], center=true);
      translate([2.5, 0.5, 6])
        cube([2, 3.5, 5], center=true);
      for (x = [-8, -1, 8]) {
        translate([x, 0, 0])
          cube([0.6, 3.2, 13], center=true);
      }
    }
    for (x = [-9.5/2, 9.5/2]) {
      translate([x, 0, 0]) rotate([90, 0, 0])
        cylinder(r=2.5/2, h=20, center=true, $fn=12);
    }
  }
}


module effector() {
  difference() {
    union() {
      cylinder(r=offset-3, h=height, center=true, $fn=60);
      for (a = [60:120:359]) rotate([0, 0, a]) {
	rotate([0, 0, 30]) translate([offset-2, 0, 0])
	  cube([10, 13, height], center=true);
	for (s = [-1, 1]) scale([s, 1, 1]) {
	  translate([0, offset, 0]) difference() {
	    intersection() {
	      cube([separation, 40, height], center=true);
	      translate([0, -4, 0]) rotate([0, 90, 0])
		cylinder(r=10, h=separation, center=true);
	      translate([separation/2-7, 0, 0]) rotate([0, 90, 0])
		cylinder(r1=cone_r2, r2=cone_r1, h=14, center=true, $fn=24);
	    }
	    rotate([0, 90, 0])
	      cylinder(r=m3_radius, h=separation+1, center=true, $fn=12);
	    rotate([90, 0, 90])
	      cylinder(r=m3_nut_radius, h=separation-24, center=true, $fn=6);
	  }
        }
      }
    }
    translate([0, 0, push_fit_height-height/2])
      cylinder(r=hotend_radius, h=height, $fn=36);
    translate([0, 0, -6]) # import("m5_internal.stl");
    for (a = [0:60:359]) rotate([0, 0, a]) {
      translate([0, mount_radius, 0])
	cylinder(r=m3_wide_radius, h=2*height, center=true, $fn=12);
    }
  }
}






module e3d() {
	difference() {
		union() {
			translate([0,0,0]) fncylinder(r=8,h=7);
			translate([0,0,6]) fncylinder(r=6,h=8);
			translate([0,0,13]) fncylinder(r=8,h=8);
			translate([0,0,20]) fncylinder(r=11.15,h=26);
			translate([0,0,0]) fncylinder(r=8,h=7);
			translate([0,0,45]) fncylinder(r=2,h=4.1);
			translate([-4.5,-4.5,49.6]) chamfercube([11.5,20,18.5],side=[0.4,0.4,0.4,0.4],top=[0.4,0.4,0.4,0.4],bottom=[0.4,0.4,0.4,0.4]);
			translate([-4.5,-4.5,48.1]) chamfercube([11.5,12,5],side=[0.4,0.4,0.4,0.4],top=[0.4,0.4,0.4,0.4],bottom=[0.4,0.4,0.4,0.4]);
			translate([0,0,67.1]) fncylinder(r=2.5,h=3);
			translate([0,0,69.1]) fncylinder(r=4.03,h=3,fn=6);
			translate([0,0,71.1]) fncylinder(r=3,r2=0.5,h=3);
		}
		if(detaillevel==1) {
			translate([0,0,-1]) fncylinder(r=1.6,h=64.1);
			translate([0,0,62.1]) fncylinder(r=0.25,h=4,fn=10);
			translate([0,0,16]) fnpipe(r=9,r2=4.475,h=1.5); 
			translate([0,0,18.5]) fnpipe(r=9,r2=4.475,h=1.5); 
			for ( i = [0 : 9] ) {
				translate([0,0,21+i*2.5]) fnpipe(r=12.15,r2=4.475+i*0.15,h=1.5); 
			}
			translate([-0.5,7.5,47.1]) fncylinder(r=3.05, h=22);
			translate([-1.5,7.5,47.1]) cube([2,8.5,22]);
			translate([-5.5,13,52.6]) rotate([0,90,0]) fncylinder(r=1.4, h=13.5);
			translate([-5.5,13,61.6]) rotate([0,90,0]) fncylinder(r=1.4, h=13.5);
			translate([-5.5,13,52.6]) rotate([0,90,0]) fncylinder(r=1.6, h=5.2);
			translate([-5.5,13,61.6]) rotate([0,90,0]) fncylinder(r=1.6, h=5.2);
			translate([8,4,59.1]) rotate([0,-90,0]) fncylinder(r=1.4, h=4.45);
			translate([8,4,62.6]) rotate([0,-90,0]) fncylinder(r=0.9, h=3);
			translate([8,4,62.6]) rotate([0,-90,0]) fncylinder(r=0.75, h=5.45);
		}
	}
}

module chamfercube(xyz=[0,0,0],side=[0,0,0,0],top=[0,0,0,0],bottom=[0,0,0,0],x=false,y=false,z=false) {
	translate([x==true?-xyz[0]/2:0,y==true?-xyz[1]/2:0,z==true?-xyz[2]/2:0]) difference() {
		cube(xyz);
		if(side[0]>=0) translate([0,0,xyz[2]/2]) rotate([0,0,45]) cube([side[0]*2,side[0]*3,xyz[2]+2],center=true);
		if(side[1]>=0) translate([xyz[0],0,xyz[2]/2]) rotate([0,0,-45]) cube([side[1]*2,side[1]*3,xyz[2]+2],center=true);
		if(side[2]>=0) translate([xyz[0],xyz[1],xyz[2]/2]) rotate([0,0,45]) cube([side[2]*2,side[2]*3,xyz[2]+2],center=true);
		if(side[3]>=0) translate([0,xyz[1],xyz[2]/2]) rotate([0,0,-45]) cube([side[3]*2,side[3]*3,xyz[2]+2],center=true);
		if(top[0]>=0) translate([xyz[0]/2,0,xyz[2]]) rotate([-45,0,0]) cube([xyz[0]+2,top[0]*2,top[0]*3,],center=true);
		if(top[2]>=0) translate([xyz[0]/2,xyz[1],xyz[2]]) rotate([45,0,0]) cube([xyz[0]+2,top[2]*2,top[2]*3,],center=true);
		if(top[3]>=0) translate([0,xyz[1]/2,xyz[2]]) rotate([0,45,0]) cube([top[3]*2,xyz[1]+2,top[3]*3,],center=true);
		if(top[1]>=0) translate([xyz[0],xyz[1]/2,xyz[2]]) rotate([0,-45,0]) cube([top[1]*2,xyz[1]+2,top[1]*3,],center=true);
		if(bottom[0]>=0) translate([xyz[0]/2,0,0]) rotate([45,0,0]) cube([xyz[0]+2,bottom[0]*2,bottom[0]*3,],center=true);
		if(bottom[2]>=0) translate([xyz[0]/2,xyz[1],0]) rotate([-45,0,0]) cube([xyz[0]+2,bottom[2]*2,bottom[2]*3,],center=true);
		if(bottom[3]>=0) translate([0,xyz[1]/2,0]) rotate([0,-45,0]) cube([bottom[3]*2,xyz[1]+2,bottom[3]*3,],center=true);
		if(bottom[1]>=0) translate([xyz[0],xyz[1]/2,0]) rotate([0,45,0]) cube([bottom[1]*2,xyz[1]+2,bottom[1]*3,],center=true);
	}	
}
module fnpipe(r,r2,h,fn){
	if (fn==undef) {
		difference() {
			fncylinder(r=r,h=h,$fn=2*r*3.14/fnresolution);
			translate([0,0,-1]) fncylinder(r=r2,h=h+2,$fn=2*r*3.14/fnresolution);
		}
	} else {
		difference() {
			fncylinder(r=r,h=h,fn=fn);
			translate([0,0,-1]) fncylinder(r=r2,h=h+2,fn=fn);
		}
	}
}

module fncylinder(r,r2,h,fn){
	if (fn==undef) {
		if (r2==undef) {
			cylinder(r=r,h=h,$fn=2*r*3.14/fnresolution);
		} else {
			cylinder(r=r,r2=r2,h=h,$fn=2*r*3.14/fnresolution);
		}
	} else {
		if (r2==undef) {
			cylinder(r=r,h=h,$fn=fn);
		} else {
			cylinder(r=r,r2=r2,h=h,$fn=fn);
		}
	}
}

